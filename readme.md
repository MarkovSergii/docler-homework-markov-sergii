**Tic Tac Toe Game**

**Install**

`npm i`

**Start**

`npm start`
or
`node index.js`

**Test**

`npm run test`

**Play**

Select in what mode you want start app
1) as server
2) as Tcp client

**For Server**

 - Select TCP port or use default from ENV or arg
 - Select WEB SOCKET port or use default from ENV or arg
 - Select HTTP port or use default from ENV or arg

**For Client**
 - Select remote address of TCP server or use default from ENV or arg
 - enter name to enter the lobby
 - select next action
    - set ready state for search opponent
    - play with AI
    - connect to active game as spectator
 - type CTRL-C to stop game and return to lobby or to exit
 
**For web client**
 - connect to own serve server
 - enter name to enter the lobby
 - select next action
    - set ready state for search opponent
    - play with AI
    - connect to active game as spectator
 