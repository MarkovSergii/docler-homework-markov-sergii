const net = require('net')
const unpack = require('../services/unpack').unpack
const ee = require('../services/events')
let tcpData = Buffer.alloc(0)

let start = (config) => {
  const client = new net.Socket()

  client.on('end', () => {
    console.log('we disconnected')
    process.exit()
  })
  // collect data from socket
  // split by protocol package and send to unpack
  client.on('data', (data) => {
    tcpData = Buffer.concat([tcpData, data])
    while (tcpData.length > config.HEADER_LENGTH) {
      try {
        const { tail, empty } = unpack(tcpData, 'tcp')
        tcpData = tail
        if (empty) return
      } catch (e) {
        console.log(`error ${e.message}`)
      }
    }
  })
  let host = config.DEFAULT_REMOTE_SERVER_ADDRESS.split(':')[0]
  let port = config.DEFAULT_REMOTE_SERVER_ADDRESS.split(':')[1]
  // TCP client connection
  client.connect(port, host, async () => {
    ee.emit('client:connected', client)
  })
}

module.exports = {
  start
}
