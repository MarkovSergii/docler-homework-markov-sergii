
let cliMenu = require('../services/cli-menu')
let tcpSendService = require('../services/tcpSendService')

// Tcp client functions for communication with server

class TcpClient {
  constructor (connection) {
    this.connection = connection
  }
  async askName () {
    let name = await cliMenu.askNameMenu()
    let netPackage = { command: 'server:setUserName', data: { name } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage, this.uuid))
  }
  setUUID (uuid) {
    this.uuid = uuid
  }
  setGameOptions (option, value) {
    this[option] = value
  }
  sendStatus (status) {
    let netPackage = { command: 'server:setUserStatus', data: { status } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage, this.uuid))
  }
  goToLobby () {
    let netPackage = { command: 'server:goToLobby', data: { } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage, this.uuid))
  }
  finishGame (gameUUID) {
    let netPackage = { command: 'server:finishGame', data: { gameUUID } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage, this.uuid))
  }
  sendMove (point) {
    let netPackage = { command: 'server:tryToMove', data: { point, gameUUID: this.gameUUID } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage, this.uuid))
  }
  askSpectGame (gameUUID) {
    let netPackage = { command: 'server:askSpectGame', data: { gameUUID } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage, this.uuid))
  }
  sendGameState (board) {
    let netPackage = { command: 'client:sendGameState', data: { board } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage, this.uuid))
  }
}

module.exports = TcpClient
