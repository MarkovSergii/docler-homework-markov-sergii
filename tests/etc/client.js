const binstruct = require('binstruct')
const net = require('net')
const ee = require('./events')
const definition = binstruct.def()
  .uint32le('magic')
  .uint32le('payload')

let tcpData = Buffer.alloc(0)

let unpack = (config, binary, clientName) => {
  if (binary.indexOf(config.MAGIC) !== 0) { throw new Error(binary + '\n does not start with MAGIC') }

  if (!binary.length || binary.length < config.HEADER_LENGTH) return { tail: binary, empty: true }
  const { payload } = definition.wrap(binary)
  if (definition.size + payload > binary.length) { return { tail: binary, empty: true } }

  const data = binary.slice(definition.size, definition.size + payload)
  const tail = binary.slice(definition.size + payload)
  let empty = !tail.length

  if (data) {
    let parsedPackage = JSON.parse(data.toString())
    ee.emit('receiveData:' + clientName, parsedPackage.data)
    setTimeout(() => {
      ee.emit('receiveData:' + clientName, parsedPackage.data)
    }, 500)
  }
  return { tail, empty }
}

let run = async (config, name) => {
  let clientName = name
  return new Promise((resolve, reject) => {
    const client = new net.Socket()

    client.on('end', () => {
      console.log('we disconnected')
      process.exit()
    })
    // collect data from socket
    // split by protocol package and send to unpack
    client.on('data', (data) => {
      tcpData = Buffer.concat([tcpData, data])
      while (tcpData.length > config.HEADER_LENGTH) {
        try {
          const { tail, empty } = unpack(config, tcpData, clientName)
          tcpData = tail
          if (empty) return
        } catch (e) {
          console.log(`error ${e.message}`)
        }
      }
    })
    let host = config.DEFAULT_REMOTE_SERVER_ADDRESS.split(':')[0]
    let port = config.DEFAULT_REMOTE_SERVER_ADDRESS.split(':')[1]
    // TCP client connection
    client.connect(port, host, async () => {
      resolve(client)
    })
  })
}

module.exports = {
  run
}
