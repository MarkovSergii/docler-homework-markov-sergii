const net = require('net')
const spawn = require('child_process').spawn
let server = null
const path = require('path')
const chalk = require('chalk')

const isPortInUse = (port) => {
  return new Promise((resolve) => {
    var server = net.createServer()
    server.once('error', function (err) {
      if (err.code === 'EADDRINUSE') {
        // port is currently in use
        resolve(true)
      }
    })
    server.once('listening', function () {
      // close the server if listening doesn't fail
      resolve(false)
      server.close()
    })
    server.listen(port)
  })
}
const run = () => {
  return Promise.race([
    new Promise((resolve, reject) => {
      setTimeout(() => {
        reject(new Error('TIMEOUT'))
      }, 15000)
    }),
    new Promise(async (resolve, reject) => {
      let portInUse = await isPortInUse(process.env.SERVER_TCP_PORT)
      if (portInUse) {
        return reject(new Error(`Port already in use`))
      }
      server = spawn('node', [path.resolve(__dirname, '..', '..', 'index.js'), '--SILENCE_MODE', 'server'])
      resolve(server)
      server.stderr.on('data', data =>
        console.log(`${chalk.red('server:')} ${data}`)
      )
      // server.stdout.on('data', data => {
      //   console.log(`${chalk.red('server:')} ${data}`)
      // })
    })
  ])
}

module.exports = {
  run, get: () => server
}
