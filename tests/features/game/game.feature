Feature: Game

  Scenario: TCP Client Game

    Then start tcp client save it to variable client1
    Then save UUID in client1 to variable client1_uuid

    Then start tcp client save it to variable client2
    Then save UUID in client2 to variable client2_uuid

#   Player with name TEST_NAME_FOR_TESTS will have turn
    Then send command server:setUserName with data { "name":"TEST_NAME_FOR_TESTS" } from client1
    Then save lobby info for client1

    Then send command server:setUserName with data { "name":"Player2" } from client2
    Then save lobby info for client2

    Then send command server:setUserStatus with data { "status": "ready" } from client1
    Then send command server:setUserStatus with data { "status": "ready" } from client2

    Then save gameUUID in client1 and client2 and receive turn in client1

    Then send command server:tryToMove with data { "point":{"row":0, "column":0}, "gameUUID": "${client1_game_uuid}" } from client1
    Then get move from server {"row":0, "column":0} in client1 and client2 and turn in client2

    Then send command server:tryToMove with data { "point":{"row":0, "column":0}, "gameUUID": "${client1_game_uuid}" } from client2
    Then get cantmove in client2

    Then send command server:tryToMove with data { "point":{"row":1, "column":1}, "gameUUID": "${client2_game_uuid}" } from client2
    Then get move from server {"row":1, "column":1} in client1 and client2 and turn in client1

    Then send command server:tryToMove with data { "point":{"row":0, "column":1}, "gameUUID": "${client1_game_uuid}" } from client1
    Then get move from server {"row":0, "column":1} in client1 and client2 and turn in client2

    Then send command server:tryToMove with data { "point":{"row":2, "column":2}, "gameUUID": "${client2_game_uuid}" } from client2
    Then get move from server {"row":2, "column":2} in client1 and client2 and turn in client1

    Then send command server:tryToMove with data { "point":{"row":0, "column":2}, "gameUUID": "${client1_game_uuid}" } from client1
    Then get move from server {"row":0, "column":2} in client1 and client2 and turn in client2

    Then get finished from server in client1 and client2

    Then close client client1
    Then close client client2
    Then close server
    Then exit
