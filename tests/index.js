const path = require('path')
require('dotenv').config({ path: path.join(__dirname, '.env') })
const _ = require('lodash')
const inquirer = require('inquirer')

const spawn = require('child_process').spawn
let tester
const testsConfig = require('./features/features.json')

const runTests = async params => {
  tester = spawn(
    path.resolve(__dirname, '../node_modules/.bin/cucumber-js'),
    params
  )
  tester.stdout.pipe(process.stdout)
  tester.stderr.pipe(process.stderr)

  tester.on('exit', code => {
    console.log('tester exit', code)
    process.exit(code)
  })
}
const resolveTestDependencies = (test, tests) => {
  const originalTestObject = testsConfig.find(testConfig => testConfig.name === test)
  if (originalTestObject.dependencies.length > 0) {
    originalTestObject.dependencies.forEach(dep => {
      tests.unshift(dep)
      tests = resolveTestDependencies(dep, tests)
    })
  }
  return _.uniq(tests)
}
const resolveTestsDependencies = (tests) => {
  tests.forEach(test => {
    tests = resolveTestDependencies(test, tests)
  })
  return tests
}
const init = async () => {
  let stepDef = ['-r', path.resolve(__dirname, 'steps', 'index.js')]
  let answers = {
    tests: testsConfig.map(test => {
      return test.name
    })
  }

  answers = await inquirer
    .prompt([
      /* Pass your questions in here */
      {
        type: 'checkbox',
        name: 'tests',
        message: 'tests',
        default: answers.tests,
        choices: answers.tests,
        validate: (answers) => {
          if (answers.length === 0) {
            return 'Please choose at least one test to run'
          } else {
            return true
          }
        }
      }
    ])
  answers.tests = resolveTestsDependencies(answers.tests)
  console.log(answers.tests)
  let params = answers.tests.map(test => path.join(__dirname, 'features', test))
  params = params.concat(stepDef)
  params = params.concat(['--format', path.resolve(__dirname, '../node_modules/cucumber-pretty')])
  runTests(params)
}
init()
