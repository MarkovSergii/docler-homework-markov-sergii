const _ = require('lodash')

let replaceVariables = (str, variables) => {
  let tmp = str
  _.mapKeys(variables, (value, key) => {
    tmp = tmp.replace(new RegExp('\\${' + key + '}', 'gm'), value)
  })
  return tmp
}

module.exports = {
  replaceVariables
}
