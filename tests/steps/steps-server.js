const Cucumber = require('cucumber')
const Then = Cucumber.Then
const expect = require('chai').expect
const serverModule = require('../etc/server')
let server = null

Then(/start TCP server/, async function() {
  server = await serverModule.run()
  return expect(!!server).to.be.true
})

Then(/wait (.+)/, async function(num) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), num)
  })
})
Then(/close server/, async function() {
  if (server && server.kill){
    server.kill()
  }
})
Then(/exit/, async function() {
  setTimeout(() => process.exit(0), 1000)
})
