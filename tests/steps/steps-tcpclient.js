const _ = require('lodash')
const Cucumber = require('cucumber')
const Then = Cucumber.Then
const expect = require('chai').expect
const tcpSendService = require('../../services/tcpSendService')
const clientModule = require('../etc/client')
const utils = require('../steps/utils')
const ee = require('../etc/events')
let variables = {}

Then(/start tcp client save it to variable (.+)/, async function(variable) {
  let config = {
    SERVER_TCP_PORT: 8787,
    SERVER_HTTP_PORT: 3000,
    SERVER_WEB_SOCKET_PORT: 3001,
    DEFAULT_REMOTE_SERVER_ADDRESS: '127.0.0.1:8787',
    MAGIC: Buffer.from([0x54, 0x54, 0x54, 0x47]),
    HEADER_LENGTH: 8 }
  variables[variable] = await clientModule.run(config, variable) // return connection

  return expect(!!variables[variable]).to.be.true
})

Then(/send command (.+) with data (.+) from (.+)/, async function(command, data, player) {
  let newData = utils.replaceVariables(data, variables)
  variables['client2_point'] = undefined
  variables['client2_point'] = undefined

  let unpackData = { command: command, data: JSON.parse(newData) }
  if (variables[player + '_uuid']) {
    unpackData.uuid = variables[player + '_uuid']
  }
  let netPackage = tcpSendService.pack(unpackData)
  variables[player].write(netPackage)
})

Then(/show variables/, async function() {
  console.log(variables)
})

Then(/close client (.+)/, async function(client) {
  variables[client].destroy()
})

Then(/save UUID in (.+) to variable (.+)/, async function(player, variable) {
  return new Promise((resolve, reject) => {
    ee.on('receiveData:' + player, (data) => {
      if (data.command === 'client:setUUID') {
        variables[variable] = data.data.uuid
        resolve()
      }
    })
  })
})

Then(/save gameUUID in (.+) and (.+) and receive turn in (.+)/, { timeout: 10 * 1000 }, function(player1, player2, player11) {
  return new Promise((resolve, reject) => {
    let checkAllCondition = () => {
      if (variables['client1_game_uuid'] &&
        variables['client2_game_uuid'] &&
        variables['client1_game_uuid'] === variables['client2_game_uuid'] &&
        variables['client1_turn']) {
        resolve()
      }
    }
    ee.on('receiveData:' + player2, (data) => {
      if (data.command === 'client:start-game') {
        variables[player2 + '_game_uuid'] = data.data.uuid
        checkAllCondition()
      }
    })
    ee.on('receiveData:' + player1, (data) => {
      if (data.command === 'client:start-game') {
        variables[player1 + '_game_uuid'] = data.data.uuid
        checkAllCondition()
      }
    })
    ee.on('receiveData:' + player1, (data) => {
      if (data.command === 'client:turn') {
        variables[player1 + '_turn'] = true
        checkAllCondition()
      }
    })
  })
})

Then(/get move from server (.+) in (.+) and (.+) and turn in (.+)/, { timeout: 10 * 1000 }, function(point, player1, player2, player22) {
  return new Promise((resolve, reject) => {
    let p = JSON.parse(point)

    let checkAllCondition = () => {
      if (variables['client1_point'] &&
          variables['client2_point'] &&
          !_.isUndefined(variables['client1_point'].row) &&
          !_.isUndefined(variables['client1_point'].column) &&
          !_.isUndefined(variables['client2_point'].row) &&
          !_.isUndefined(variables['client2_point'].column) &&
          variables['client1_point'].row === variables['client2_point'].row &&
          variables['client2_point'].row === p.row &&
          variables['client1_point'].column === variables['client2_point'].column &&
          variables['client2_point'].column === p.column &&
          variables[player22 + '_turn']
      ) {
        resolve()
      }
    }
    ee.on('receiveData:' + player1, (data) => {
      if (data.command === 'client:move') {
        variables[player1 + '_point'] = data.data.point
        checkAllCondition()
      }
    })
    ee.on('receiveData:' + player2, (data) => {
      if (data.command === 'client:move') {
        variables[player2 + '_point'] = data.data.point
        checkAllCondition()
      }
    })
    ee.on('receiveData:' + player22, (data) => {
      if (data.command === 'client:turn') {
        variables[player22 + '_turn'] = true
        checkAllCondition()
      }
    })
  })
})

Then(/get finished from server in (.+) and (.+)/, { timeout: 10 * 1000 }, function(player1, player2) {
  return new Promise((resolve, reject) => {
    let checkAllCondition = () => {
      if (variables['client1_finished'] &&
          variables['client2_finished']) {
        resolve()
      }
    }
    ee.on('receiveData:' + player1, (data) => {
      if (data.command === 'client:finished') {
        variables[player1 + '_finished'] = true
        checkAllCondition()
      }
    })
    ee.on('receiveData:' + player2, (data) => {
      if (data.command === 'client:finished') {
        variables[player2 + '_finished'] = true
        checkAllCondition()
      }
    })
  })
})

Then(/save lobby info for (.+)/, { timeout: 10 * 1000 }, async function(player) {
  return new Promise((resolve, reject) => {
    ee.on('receiveData:' + player, (data) => {
      if (data.command === 'client:lobbyInfo') {
        resolve()
      }
    })
  })
})

Then(/get turn command in (.+)/, async function(player) {
  return new Promise((resolve, reject) => {
    ee.on('receiveData:' + player, (data) => {
      if (data.command === 'client:turn') {
        resolve()
      }
    })
  })
})

Then(/get cantmove in (.+)/, async function(player) {
  return new Promise((resolve, reject) => {
    ee.on('receiveData:' + player, (data) => {
      if (data.command === 'client:cantmove') {
        resolve()
      }
    })
  })
})
