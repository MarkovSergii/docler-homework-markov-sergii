const http = require('http')
const url = require('url')
const fs = require('fs')
const path = require('path')

let start = (config) => {
  const mimeType = {
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.css': 'text/css',
    '.png': 'image/png',
    '.jpg': 'image/jpeg'
  }
  // creating http server for serve client
  http.createServer(async function (req, res) {
    let parsedUrl = url.parse(req.url) //eslint-disable-line

    // send index if url '/'
    if (parsedUrl.pathname === '/') {
      parsedUrl.pathname = '/index.html'
    }
    // send current web socket port
    if (parsedUrl.pathname === '/web-socket-port') {
      res.setHeader('Content-type', 'text/plain')
      res.end(config.SERVER_WEB_SOCKET_PORT.toString())
    }
    // prevent send server inner files
    const sanitizePath = path.normalize(parsedUrl.pathname).replace(/^(\.\.[/\\])+/, '')
    let pathname = path.join(__dirname, 'static', sanitizePath)

    if (await fs.existsSync(pathname)) {
      try {
        // send file if exist
        const ext = path.parse(pathname).ext
        res.setHeader('Content-type', mimeType[ext] || 'text/plain')
        fs.createReadStream(pathname).pipe(res)
      } catch (e) {
        // error on reading or sending file
        res.statusCode = 500
        res.end(`Server error: reading file ${pathname}`)
      }
    } else {
      // file not found
      res.statusCode = 404
      res.end(`File ${pathname} not found!`)
    }
  }).listen(parseInt(config.SERVER_HTTP_PORT))

  console.log(`Server Http listening on port ${config.SERVER_HTTP_PORT}`)
}

module.exports = {
  start
}
