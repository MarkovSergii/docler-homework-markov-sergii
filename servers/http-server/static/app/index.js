/* global angular alert  WebSocket */
let ticTakToe = angular.module('ticTakToe', [])

let mainController = ($scope, $http) => {
  $scope.userInputWebSocketAddress = null

  $scope.sendToWebSocket = (command, data) => {
    let sendData
    if ($scope.uuid) {
      sendData = {
        uuid: $scope.uuid,
        data: data
      }
    } else {
      sendData = {
        data: data
      }
    }
    $scope.socket.send(JSON.stringify({ command, data: sendData }))
  }

  $scope.cellClick = (row, column) => {
    if ($scope.spectMode || !$scope.ourTurn) {
      return
    }
    $scope.sendToWebSocket('server:tryToMove', { gameUUID: $scope.gameUUID, point: { row, column } })
  }

  $scope.readyToPlay = () => {
    $scope.status = 'Search opponent'
    $scope.sendToWebSocket('server:setUserStatus', { status: 'ready' })
  }

  $scope.playWithAI = () => {
    $scope.status = 'Search opponent'
    $scope.sendToWebSocket('server:setUserStatus', { status: 'ai' })
  }

  $scope.spectate = () => {
    $scope.state = 'askSpec'
    $scope.sendToWebSocket('server:setUserStatus', { status: 'spectator' })
  }

  $scope.sendUserName = (userName) => {
    if (!userName) {
      alert('Need enter name')
      return
    }
    $scope.sendToWebSocket('server:setUserName', { name: userName })
  }

  $scope.goToLobby = () => {
    $scope.state = 'lobby'
  }

  $scope.startSpectGame = (gameUUID) => {
    $scope.sendToWebSocket('server:askSpectGame', { gameUUID })
  }

  // list commands from server
  $scope.commands = {
    'client:setUUID': (data) => {
      $scope.uuid = data.uuid
      $scope.$apply()
    },
    'client:lobbyInfo': (data) => {
      $scope.lobbyInfo = data.lobbyInfo
      $scope.state = 'lobby'
      $scope.$apply()
    },
    'client:start-game': (data) => {
      $scope.state = 'game'
      $scope.status = `game started with ${data.opponentName}`
      $scope.spectMode = false
      $scope.board = [[null, null, null],
        [null, null, null],
        [null, null, null] ]
      $scope.ourTurn = false
      $scope.gameUUID = data.uuid
      $scope.sign = data.sign
      $scope.$apply()
    },
    'client:turn': (data) => {
      $scope.ourTurn = true
      $scope.$apply()
    },
    'client:move': (data) => {
      $scope.board[data.point.row][data.point.column] = data.sign
      $scope.ourTurn = false

      $scope.$apply()
    },
    'client:sendSpectEnd': (data) => {
      setTimeout(() => {
        if (data.result.state === 'draw') {
          alert('draw')
        }
        if (data.result.state === 'winner') {
          alert(data.result.winner + ' ' + data.result.state)
        }
        if (data.result.state === 'disconnect') {
          alert('Game over. Opponent disconnected')
        }
        $scope.status = ''
        $scope.state = 'lobby'
        $scope.$apply()
      }, 100)
    },
    'client:cantmove': (data) => {
      alert('Cell is busy try other')
    },
    'client:active-games': (data) => {
      $scope.activeGames = data.activeGames
      $scope.$apply()
    },
    'client:spectStart': (data) => {
      $scope.state = 'spec'
      $scope.spectMode = true
      $scope.$apply()
    },
    'client:sendGameState': (data) => {
      $scope.board = data.board
      $scope.$apply()
    },
    'client:finished': (data) => {
      setTimeout(() => {
        if (data.result.state === 'draw') {
          alert('draw')
        }
        if (data.result.state === 'winner') {
          alert(data.result.winner + ' ' + data.result.state)
        }
        if (data.result.state === 'disconnect') {
          alert('Game over. Opponent disconnected')
        }
        $scope.status = ''
        $scope.state = 'lobby'
        $scope.$apply()
      }, 100)
    }
  }

  $scope.createWebSocketClient = (address) => {
    $scope.socket = new WebSocket(`ws://${address}`)
    $scope.socket.onopen = function() {
      $scope.state = 'login'
      $scope.connected = true
    }

    $scope.socket.onclose = function(event) {
      if (!event.wasClean) {
        $scope.connected = false
        $scope.$apply()
        alert('Server disconnected. Refresh page.')
      }
    }

    $scope.socket.onmessage = function(event) {
      let parcedData = JSON.parse(event.data)
      if ($scope.commands[parcedData.command]) {
        // execute command if exist
        $scope.commands[parcedData.command](parcedData.data)
      } else {
        console.log(parcedData.command, 'not found')
      }
    }

    $scope.socket.onerror = function(error) {
      if (error) {
        console.log(error)
      }
      // alert('Ошибка ' + error.message)
    }
  }

  $scope.start = async () => {
    try {
      let response = await $http.get('/web-socket-port')
      $scope.defaultWebSocketPort = response && response.data ? response.data : null
    } catch (e) {
      alert(e)
    }
    $scope.webSocketAddress = $scope.userInputWebSocketAddress || `localhost:${$scope.defaultWebSocketPort}`
    $scope.createWebSocketClient($scope.webSocketAddress)
  }

  $scope.start()
}

ticTakToe.controller('mainController', mainController)
