const WebSocket = require('ws')
const ee = require('../../services/events')

let start = (config) => {
  const wss = new WebSocket.Server({ port: config.SERVER_WEB_SOCKET_PORT })
  // Web socket client connection
  wss.on('connection', function connection(ws) {
    ee.emit('server:new-web-connection', ws)

    ws.on('message', (data) => {
      let parcedData = JSON.parse(data)
      ee.emit(parcedData.command, parcedData.data)
    })
  })
  console.log(`Server Web Socket listening on port ${config.SERVER_WEB_SOCKET_PORT}`)
}

module.exports = {
  start
}
