const net = require('net')
const unpack = require('../../services/unpack').unpack
const ee = require('../../services/events')

let start = (config) => {
  let server = net.createServer((connection) => {
    // collect data from socket
    // split by protocol package and send to unpack
    let tcpData = Buffer.alloc(0)
    connection.on('data', (data) => {
      tcpData = Buffer.concat([tcpData, data])
      while (tcpData.length > config.HEADER_LENGTH) {
        try {
          const { tail, empty } = unpack(tcpData, connection)
          tcpData = tail
          if (empty) return
        } catch (e) {
          console.log(`error ${e.message}`)
        }
      }
    })
  })

  server.on('connection', (connection) => {
    ee.emit('server:new-tcp-connection', connection)
  })
  server.on('error', () => console.log('server error'))

  server.listen({
    host: 'localhost',
    port: config.SERVER_TCP_PORT
  }, console.log(`Server TCP listening on port ${config.SERVER_TCP_PORT}`))
}

module.exports = {
  start
}
