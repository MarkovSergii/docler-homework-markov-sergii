const config = require('./../services/config')

const createUint32Buffer = (value) => {
  const buf = Buffer.alloc(4)
  buf.writeUInt32LE(value)
  return buf
}
// function for pack data into TCP package
const pack = (data, uuid) => {
  if (uuid) {
    data.uuid = uuid
  }
  let binData = Buffer.from(JSON.stringify({ data }))
  let binLength = createUint32Buffer(binData.length)
  return Buffer.concat([config.MAGIC, binLength, binData])
}
// function for send TCP package
const write = (connection, data) => {
  try {
    connection.write(data)
  } catch (e) {
    console.log(e)
  }
}

module.exports = {
  pack,
  write
}
