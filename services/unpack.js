const binstruct = require('binstruct')
const ee = require('./../services/events')
const config = require('./../services/config')
const definition = binstruct.def()
  .uint32le('magic')
  .uint32le('payload')

// function for unpack TCP package to command and data then send it as event
let unpack = (binary) => {
  if (binary.indexOf(config.MAGIC) !== 0) { throw new Error(binary + '\n does not start with MAGIC') }

  if (!binary.length || binary.length < config.HEADER_LENGTH) return { tail: binary, empty: true }
  const { payload } = definition.wrap(binary)
  if (definition.size + payload > binary.length) { return { tail: binary, empty: true } }

  const data = binary.slice(definition.size, definition.size + payload)
  const tail = binary.slice(definition.size + payload)
  let empty = !tail.length

  if (data) {
    let parsedPackage = JSON.parse(data.toString())
    if (parsedPackage.data.uuid) {
      ee.emit(parsedPackage.data.command, { data: parsedPackage.data.data, uuid: parsedPackage.data.uuid })
    } else {
      ee.emit(parsedPackage.data.command, { data: parsedPackage.data.data })
    }
  }
  return { tail, empty }
}

module.exports = {
  unpack
}
