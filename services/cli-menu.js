const _ = require('lodash')
const inquirer = require('inquirer')
const utilsService = require('./utils')
const board = require('./board')

let mainMenu = async (config) => {
  if (config.SILENCE_MODE === 'client' || config.SILENCE_MODE === 'server') {
    return {
      type: config.SILENCE_MODE
    }
  } else {
    let mainAnswers = await inquirer.prompt([
      {
        type: 'list',
        name: 'userChoice',
        message: 'What do you want to start?',
        choices: ['Server', 'Client', new inquirer.Separator(), 'Exit']
      }
    ])
    if (mainAnswers && mainAnswers.userChoice) {
      if (mainAnswers.userChoice === 'Server') {
        return serverMenu(config)
      }
      if (mainAnswers.userChoice === 'Client') {
        return clientMenu(config)
      }
      if (mainAnswers.userChoice === 'Exit') {
        console.log('Bye-bye')
        process.exit(0)
      }
    } else {
      console.log('Unknown action', mainAnswers.userChoice)
      return mainMenu(config)
    }
  }
}
let serverMenu = async (config) => {
  let serverAnswers = await inquirer.prompt([
    {
      type: 'input',
      name: 'serverTcpPort',
      message: 'Enter TCP server port (leave blank for use defaults)?',
      default: config.SERVER_TCP_PORT
    },
    {
      type: 'input',
      name: 'serverHttpPort',
      message: 'Enter Http server port (leave blank for use defaults)?',
      default: config.SERVER_HTTP_PORT
    },
    {
      type: 'input',
      name: 'serverWebSocketPort',
      message: 'Enter WebSocket server port (leave blank for use defaults)?',
      default: config.SERVER_WEB_SOCKET_PORT
    }
  ])
  if (serverAnswers) {
    try {
      if (!utilsService.isPortCorrect(serverAnswers.serverTcpPort)) {
        throw 'bad server TCP port' + serverAnswers.serverTcpPort + ' try again' //eslint-disable-line
      }
      if (!utilsService.isPortCorrect(serverAnswers.serverHttpPort)) {
        throw 'bad server HTTP port' + serverAnswers.serverHttpPort + ' try again' //eslint-disable-line
      }
      if (!utilsService.isPortCorrect(serverAnswers.serverWebSocketPort)) {
        throw 'bad server WebSocket port' + serverAnswers.serverWebSocketPort + ' try again' //eslint-disable-line
      }
      config.SERVER_TCP_PORT = parseInt(serverAnswers.serverTcpPort)
      config.SERVER_HTTP_PORT = parseInt(serverAnswers.serverHttpPort)
      config.SERVER_WEB_SOCKET_PORT = parseInt(serverAnswers.serverWebSocketPort)
      return {
        type: 'server'
      }
    } catch (e) {
      console.log(e)
      return serverMenu(config)
    }
  } else {
    return {
      type: 'server'
    }
  }
}
let clientMenu = async (config) => {
  let clientAnswers = await inquirer.prompt([
    {
      type: 'input',
      name: 'remoteServerAddress',
      message: 'Enter remote server address (leave blank for use defaults)?',
      default: config.DEFAULT_REMOTE_SERVER_ADDRESS
    }
  ])
  if (clientAnswers && clientAnswers.remoteServerAddress) {
    let serverAddress = clientAnswers.remoteServerAddress.split(':')
    try {
      if (!utilsService.isIP4Correct(serverAddress[0]) || !utilsService.isPortCorrect(serverAddress[1])) {
        throw 'bad server address' + clientAnswers.remoteServerAddress+ ' try again' //eslint-disable-line
      }
      config.SERVER_TCP_PORT = clientAnswers.remoteServerAddress
      return {
        type: 'client'
      }
    } catch (e) {
      console.log(e)
      return clientMenu(config)
    }
  } else {
    return {
      type: 'client'
    }
  }
}
let askNameMenu = async () => {
  let result = await inquirer.prompt([
    {
      type: 'input',
      name: 'playerName',
      message: 'Enter name'
    }
  ])
  if (result && result.playerName) {
    return result.playerName
  } else {
    return askNameMenu()
  }
}
let lobbyMenu = async (lobbyInfo) => {
  let mainAnswers = await inquirer.prompt([
    {
      type: 'list',
      name: 'userChoice',
      message: `You are in lobby. Total players in lobby (${lobbyInfo}). Select next action`,
      choices: ['Search opponent', 'Play with AI', 'Spectator', new inquirer.Separator(), 'Exit']
    }
  ])
  if (mainAnswers && mainAnswers.userChoice) {
    if (mainAnswers.userChoice === 'Search opponent') {
      return 'ready'
    }
    if (mainAnswers.userChoice === 'Play with AI') {
      return 'ai'
    }
    if (mainAnswers.userChoice === 'Spectator') {
      return 'spectator'
    }
    if (mainAnswers.userChoice === 'Exit') {
      console.log('Bye-bye')
      process.exit(0)
    }
  } else {
    console.log('Unknown action', mainAnswers.userChoice)
    return lobbyMenu(lobbyInfo)
  }
}
let selectGameSpectMenu = async (games, lobbyInfo) => {
  let mainAnswers = await inquirer.prompt([
    {
      type: 'list',
      name: 'userChoice',
      message: `Select game for spectator`,
      choices: _.map(games, 'playersNames').map((game, i) => (i + 1 + ') ' + game)).concat('Exit')
    }
  ])
  if (mainAnswers && mainAnswers.userChoice) {
    if (mainAnswers.userChoice === 'Exit') {
      return lobbyMenu('')
    } else {
      console.log(mainAnswers && mainAnswers.userChoice)
      let num = mainAnswers.userChoice.indexOf(')')
      let index = mainAnswers.userChoice.slice(0, num)
      return games[index - 1].uuid
    }
  } else {
    console.log('Unknown action', mainAnswers.userChoice)
    return selectGameSpectMenu(games)
  }
}

let showBoard = (opponentName, gameUUID) => {
  board.drawBoard(opponentName, gameUUID)
}

let setTurn = () => {
  board.setTurn()
}

let setSign = (...arg) => {
  board.setSign(...arg)
}
let cantmove = () => {
  board.cantmove()
}
let finished = (...arg) => {
  board.finished(...arg)
}
let setBoard = (...arg) => {
  board.setBoard(...arg)
}
let spectEnd = (...arg) => {
  board.spectEnd(...arg)
}
let spectStart = (...arg) => {
  board.spectStart(...arg)
}

module.exports = {
  mainMenu,
  clientMenu,
  serverMenu,
  lobbyMenu,
  askNameMenu,
  selectGameSpectMenu,
  showBoard,
  setTurn,
  cantmove,
  setSign,
  finished,
  setBoard,
  spectEnd,
  spectStart
}
