const _ = require('lodash')
const utilsService = require('./utils')
const commander = require('commander')
require('dotenv').config()

const optionsList = [
  {
    name: 'SILENCE_MODE',
    mask: '["client"/"server"]',
    description: 'Run in silence mode with out question with defaults values',
    defaultValue: ''
  },
  {
    name: 'SERVER_TCP_PORT',
    mask: '[8787]',
    description: 'Port for local TCP server',
    defaultValue: 8787
  },
  {
    name: 'SERVER_HTTP_PORT',
    mask: '[3000]',
    description: 'Port for http server',
    defaultValue: 3000
  },
  {
    name: 'SERVER_WEB_SOCKET_PORT',
    mask: '[3001]',
    description: 'Port for web-socket server',
    defaultValue: 3001
  },
  {
    name: 'DEFAULT_REMOTE_SERVER_ADDRESS',
    mask: '[127.0.0.1:8787]',
    description: 'Default remote server address',
    defaultValue: '127.0.0.1:8787'
  }
]

optionsList.map((option) =>
  commander.option(`--${option.name} ${option.mask}`, option.description, option.defaultValue))

commander.parse(process.argv)

let config = {}

let paramTypeConvertor = (param, value) => {
  switch (param) {
    case 'SERVER_TCP_PORT':
    case 'SERVER_HTTP_PORT':
    case 'SERVER_WEB_SOCKET_PORT':
      try {
        if (utilsService.isPortCorrect(value)) {
          return parseInt(value)
        } else {
          throw `Bad port value ${value} ` //eslint-disable-line
        }
      } catch (e) {
        console.log(e)
        process.exit(0)
      }
  }
  return value
}

optionsList.forEach((paramObject) => {
  if (!_.isUndefined(process.env[paramObject.name])) {
    config[paramObject.name] = paramTypeConvertor(paramObject.name, process.env[paramObject.name])
  }

  if (!_.isUndefined(commander[paramObject.name])) {
    config[paramObject.name] = paramTypeConvertor(paramObject.name, commander[paramObject.name])
  }
  if (_.isUndefined(config[paramObject.name])) {
    config[paramObject.name] = paramObject.defaultValue
  }
})

// protocol header
config.MAGIC = Buffer.from([0x54, 0x54, 0x54, 0x47])
// total protocol header length MAGIC + 4 bit int as data length
config.HEADER_LENGTH = 8

module.exports = config
