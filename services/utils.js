const net = require('net')

let isIP4Correct = (value) => net.isIPv4(value)

let isPortCorrect = (value) =>
  (/^\d+$/.test(value) && parseInt(value) < 65535 && parseInt(value) > 1)

module.exports = {
  isIP4Correct, isPortCorrect
}
