const EventEmitter = require('events')
const emitter = new EventEmitter()
const eventsHandlerServer = require('./events-handler-server')
const eventsHandlerTcpClient = require('./events-handler-tcp-client')

// event list

emitter.on('move', eventsHandlerTcpClient.onMakeMove)

emitter.on('finishGame', eventsHandlerTcpClient.onFinishGame)

emitter.on('goToLobby', eventsHandlerTcpClient.onGoToLobby)

emitter.on('client:start-game', eventsHandlerTcpClient.onClientStartGame)

emitter.on('client:turn', eventsHandlerTcpClient.onClientTurn)

emitter.on('client:finished', eventsHandlerTcpClient.onClientFinished)

emitter.on('client:move', eventsHandlerTcpClient.onClientMove)

emitter.on('client:cantmove', eventsHandlerTcpClient.onClientCantMove)

emitter.on('client:active-games', eventsHandlerTcpClient.onClientActiveGames)

emitter.on('client:connected', (eventsHandlerTcpClient.onClientConnected))

emitter.on('client:setUUID', eventsHandlerTcpClient.onClientSetUUID)

emitter.on('client:sendGameState', eventsHandlerTcpClient.onClientSendGameState)

emitter.on('client:sendSpectEnd', eventsHandlerTcpClient.onClientSendSpectEnd)

emitter.on('client:spectStart', eventsHandlerTcpClient.onClientSpectStart)

emitter.on('client:lobbyInfo', eventsHandlerTcpClient.onClientLobbyInfo)

// --------- SERVER

emitter.on('server:new-tcp-connection', eventsHandlerServer.onNewTcpConnection)

emitter.on('server:new-web-connection', eventsHandlerServer.onNewWebConnection)

emitter.on('server:setUserName', eventsHandlerServer.onSetUserName)

emitter.on('server:setUserStatus', eventsHandlerServer.onSetUserStatus)

emitter.on('server:tryToMove', eventsHandlerServer.onTryToMove)

emitter.on('server:goToLobby', eventsHandlerServer.onGoToLobby)

emitter.on('server:finishGame', eventsHandlerServer.onFinishGame)

emitter.on('server:askSpectGame', eventsHandlerServer.onAskSpectGame)

module.exports = emitter
