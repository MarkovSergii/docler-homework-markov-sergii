const chalk = require('chalk')
const charm = require('charm')()
charm.pipe(process.stdout)
charm.reset()
const OUTPUT_LINES_COUNT = 14
const LINE_LENGTH = 60
let board = [[null, null, null],
  [null, null, null],
  [null, null, null] ]
let ourMove = false
let opponentName = ''
let gameUUID = ''
let selected = {
  row: 0,
  column: 0
}
let keyListen = false
let spectMode = false
let getValueOfCell = (row, column) => {
  return board[row][column] || ' '
}

let isSelected = (row, column) =>
  ourMove ? selected.row === row && selected.column === column : false

let buildDisplayString = (msg) => {
  return `${msg}
 ${isSelected(0, 0) ? '┌───┐' : '     '} │ ${isSelected(0, 1) ? '┌───┐' : '     '} │ ${isSelected(0, 2) ? '┌───┐' : '     '}
 ${isSelected(0, 0) ? '│' : ' '} ${getValueOfCell(0, 0)} ${isSelected(0, 0) ? '│' : ' '} │ ${isSelected(0, 1) ? '│' : ' '} ${getValueOfCell(0, 1)} ${isSelected(0, 1) ? '│' : ' '} │ ${isSelected(0, 2) ? '│' : ' '} ${getValueOfCell(0, 2)} ${isSelected(0, 2) ? '│' : ' '}
 ${isSelected(0, 0) ? '└───┘' : '     '} │ ${isSelected(0, 1) ? '└───┘' : '     '} │ ${isSelected(0, 2) ? '└───┘' : '     '}
 ───── │ ───── │ ─────
 ${isSelected(1, 0) ? '┌───┐' : '     '} │ ${isSelected(1, 1) ? '┌───┐' : '     '} │ ${isSelected(1, 2) ? '┌───┐' : '     '}
 ${isSelected(1, 0) ? '│' : ' '} ${getValueOfCell(1, 0)} ${isSelected(1, 0) ? '│' : ' '} │ ${isSelected(1, 1) ? '│' : ' '} ${getValueOfCell(1, 1)} ${isSelected(1, 1) ? '│' : ' '} │ ${isSelected(1, 2) ? '│' : ' '} ${getValueOfCell(1, 2)} ${isSelected(1, 2) ? '│' : ' '}
 ${isSelected(1, 0) ? '└───┘' : '     '} │ ${isSelected(1, 1) ? '└───┘' : '     '} │ ${isSelected(1, 2) ? '└───┘' : '     '}
 ───── │ ───── │ ─────       
 ${isSelected(2, 0) ? '┌───┐' : '     '} │ ${isSelected(2, 1) ? '┌───┐' : '     '} │ ${isSelected(2, 2) ? '┌───┐' : '     '}
 ${isSelected(2, 0) ? '│' : ' '} ${getValueOfCell(2, 0)} ${isSelected(2, 0) ? '│' : ' '} │ ${isSelected(2, 1) ? '│' : ' '} ${getValueOfCell(2, 1)} ${isSelected(2, 1) ? '│' : ' '} │ ${isSelected(2, 2) ? '│' : ' '} ${getValueOfCell(2, 2)} ${isSelected(2, 2) ? '│' : ' '}
 ${isSelected(2, 0) ? '└───┘' : '     '} │ ${isSelected(2, 1) ? '└───┘' : '     '} │ ${isSelected(2, 2) ? '└───┘' : '     '}
Type Ctrl-C to return to lobby
`
}

let redrawCells = (msg = chalk.green('Your turn')) => {
  charm.up(OUTPUT_LINES_COUNT)
  while (msg.length < LINE_LENGTH) {
    msg = msg + ' '
  }
  console.log(buildDisplayString(msg))
}

let move = (direction) => {
  switch (direction) {
    case 'up':
      if (selected.row > 0) {
        selected.row--
        redrawCells()
      }
      break
    case 'down':
      if (selected.row < 2) {
        selected.row++
        redrawCells()
      }
      break
    case 'left':
      if (selected.column > 0) {
        selected.column--
        redrawCells()
      }
      break
    case 'right':
      if (selected.column < 2) {
        selected.column++
        redrawCells()
      }
      break
  }
}

let setSign = (row, column, sign) => {
  board[row][column] = sign
  ourMove = false
  redrawCells(chalk.yellow('Opponent turn'))
}

let cantmove = () => {
  redrawCells(chalk.red('Cell is busy try other'))
}

let setTurn = () => {
  ourMove = true
  redrawCells(chalk.green('Your turn'))
}
let finished = (result) => {
  ourMove = false
  if (result.state === 'draw') {
    redrawCells(chalk.yellow(`Game over. Draw try again`))
  }
  if (result.state === 'disconnect') {
    redrawCells(chalk.red(`Game over. Opponent disconnected`))
  }
  if (result.state === 'winner') {
    redrawCells(chalk.green(`Game over. Player ${result.winner} win`))
  }
}

let setBoard = (b) => {
  board = b
  redrawCells(chalk.blue(`Spectator mode`))
}
let spectEnd = () => {
  redrawCells(chalk.blue('Spectator mode finished'))
}

let drawBoard = (opponent, uuid) => {
  spectMode = !opponent
  let ee = require('./events')
  const keypress = require('keypress')
  keypress(process.stdin)

  let chooseSign = () => {
    ee.emit('move', selected)
  }
  if (!keyListen) {
    keyListen = true
    process.stdin.on('keypress', function (ch, key) {
      if (key && key.ctrl && key.name === 'c') {
        if (spectMode) {
          ee.emit('goToLobby')
          return
        }
        ee.emit('finishGame', gameUUID)
        ee.emit('goToLobby')
      }
      if (ourMove && key && (key.name === 'return' || key.name === 'space')) {
        chooseSign()
      }
      if (ourMove && key && (key.name === 'up' || key.name === 'down' || key.name === 'left' || key.name === 'right')) {
        move(key.name)
      }
    })
  }

  process.stdin.setRawMode(true)
  process.stdin.resume()
  opponentName = opponent
  gameUUID = uuid
  selected = {
    row: 0,
    column: 0
  }
  ourMove = false
  board = [[null, null, null],
    [null, null, null],
    [null, null, null] ]

  console.log(buildDisplayString(chalk.green(`Game started. Opponent Name: ${opponentName}`)))
}

let spectStart = () => {
  drawBoard()
}

module.exports = {
  drawBoard,
  setSign,
  setTurn,
  cantmove,
  finished,
  setBoard,
  spectEnd,
  spectStart

}
