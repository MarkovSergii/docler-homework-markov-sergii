const core = require('../core/core')
const connections = require('../core/connections')

// event handlers for server

const onNewTcpConnection = (connection) => {
  let client = new connections.ClientTcp(connection)
  core.addClient(client)
  connection.on('close', (e) => {
    core.closeGamesFor(client.getUUID())
    console.log('client close connection')
  })
}

const onNewWebConnection = (connection) => {
  let client = new connections.ClientWeb(connection)
  core.addClient(client)
  connection.on('close', (e) => {
    core.closeGamesFor(client.getUUID())
    core.removeFromLobby(client.getUUID())
    core.removeClient(client.getUUID())
    console.log('client close connection')
  })
}

const onSetUserName = (pack) => {
  let client = core.getClient(pack.uuid)
  client.setName(pack.data.name)
  core.addToLobby(pack.uuid)
  client.sendLobbyInfo()
}

const onSetUserStatus = (pack) => {
  let client = core.getClient(pack.uuid)
  if (pack.data.status === 'spectator') {
    client.sendActiveGames()
  } else {
    core.setStatus(pack.uuid, pack.data.status)
  }
}

const onTryToMove = (pack) => {
  let GameObj = core.getGame(pack.data.gameUUID)
  if (GameObj) {
    GameObj.game.tryToMove(pack.uuid, pack.data.point)
  }
}

const onGoToLobby = (pack) => {
  let client = core.getClient(pack.uuid)
  core.addToLobby(pack.uuid)
  client.sendLobbyInfo()
}

const onFinishGame = (pack) => {
  core.deleteGame(pack.data.gameUUID)
}

const onAskSpectGame = (pack) => {
  core.addToSpect(pack.data.gameUUID, pack.uuid)
}

module.exports = {
  onNewTcpConnection,
  onNewWebConnection,
  onSetUserName,
  onSetUserStatus,
  onTryToMove,
  onFinishGame,
  onGoToLobby,
  onAskSpectGame
}
