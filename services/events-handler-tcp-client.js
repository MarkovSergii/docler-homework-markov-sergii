const cliMenu = require('../services/cli-menu')
let TcpPlayer = require('../tcp-client/tcp-player')
let tcpClient = null

// event handlers for tcp client

const onMakeMove = (point) => {
  tcpClient.sendMove(point)
}

const onFinishGame = (gameUUID) => {
  tcpClient.finishGame(gameUUID)
}

const onGoToLobby = (point) => {
  tcpClient.goToLobby()
}

const onClientStartGame = (pack) => {
  tcpClient.setGameOptions('gameUUID', pack.data.uuid)
  tcpClient.setGameOptions('gameSign', pack.data.sign)
  tcpClient.setGameOptions('opponentName', pack.data.opponentName)
  cliMenu.showBoard(pack.data.opponentName, pack.data.uuid)
}

const onClientTurn = (pack) => {
  cliMenu.setTurn()
}

const onClientFinished = async (pack) => {
  cliMenu.finished(pack.data.result)
}

const onClientMove = (pack) => {
  cliMenu.setSign(pack.data.point.row, pack.data.point.column, pack.data.sign)
}

const onClientCantMove = async (pack) => {
  cliMenu.cantmove()
}

const onClientActiveGames = async (pack) => {
  let gameUUID = await cliMenu.selectGameSpectMenu(pack.data.activeGames)
  tcpClient.askSpectGame(gameUUID)
}

const onClientConnected = (connection) => {
  tcpClient = new TcpPlayer(connection)
  tcpClient.askName()
}

const onClientSetUUID = (pack) => {
  tcpClient.setUUID(pack.data.uuid)
}

const onClientSendGameState = (pack) => {
  cliMenu.setBoard(pack.data.board)
}
const onClientSendSpectEnd = (pack) => {
  cliMenu.spectEnd()
}
const onClientSpectStart = (pack) => {
  cliMenu.spectStart()
}

const onClientLobbyInfo = async (pack) => {
  let answer = await cliMenu.lobbyMenu(pack.data.lobbyInfo)
  tcpClient.sendStatus(answer)
}

module.exports = {
  onMakeMove,
  onFinishGame,
  onGoToLobby,
  onClientStartGame,
  onClientTurn,
  onClientFinished,
  onClientMove,
  onClientCantMove,
  onClientActiveGames,
  onClientConnected,
  onClientSetUUID,
  onClientSendGameState,
  onClientSendSpectEnd,
  onClientSpectStart,
  onClientLobbyInfo
}
