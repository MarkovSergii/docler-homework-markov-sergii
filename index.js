const config = require('./services/config')
const cliMenu = require('./services/cli-menu')

const webSocket = require('./servers/web-socket/web-socket')
const tcpServer = require('./servers/tcp-server/tcp-server')
const httpServer = require('./servers/http-server/http-server')

const tcpClient = require('./tcp-client/tcp-client')

let start = async () => {
  let mode = await cliMenu.mainMenu(config)
  if (mode && mode.type && mode.type === 'server') {
    // start as server
    await webSocket.start(config)
    await tcpServer.start(config)
    await httpServer.start(config)
  }

  if (mode && mode.type && (mode.type === 'client' || mode.type === 'spectator')) {
    // start as TCP client
    await tcpClient.start(config)
  }
}
 // start main fn
start()
