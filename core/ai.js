const _ = require('lodash')
// very simple Ai based on random :)
class AI {
  constructor (game) {
    this.game = game
  }
  getName() {
    return 'Tic Tac Toe Bot'
  }
  startGame(uuid, sign, opponentName) {
    this.gameUUID = uuid
    this.sign = sign
    return 'Tic Tac Toe Bot'
  }
  turn() {
    let found = false
    let point
    while (found === false) {
      let row = _.random(0, 2)
      let column = _.random(0, 2)
      found = this.game.canSignCell({ row, column })
      if (found) {
        point = { row, column }
      }
    }
    this.game.tryToMove('Tic Tac Toe Bot', point)
  }
  move(point) {}

  finished(result) {}

  getUUID() {
    return 'Tic Tac Toe Bot'
  }
}

module.exports = AI
