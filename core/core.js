let Game = require('./game')
class Core {
  constructor () {
    this.clients = new Map()
    this.lobby = new Map()
    this.games = new Map()
    this.READY_CHECK_INTERVAL = 3000
    this.DELETE_FINISHED_GAMES_CHECK_INTERVAL = 2000
    // check every 3000 sec to players in ready and ai mode
    // if found create game instance
    this.readyChecker = setInterval(() => {
      let firstPlayer = null
      let secondPlayer = null
      this.lobby.forEach((value, key) => {
        if (value === 'ready') {
          if (!firstPlayer) {
            // found first player
            firstPlayer = key
          } else {
            // found second player
            secondPlayer = key
            // create game instance
            let newGame = new Game(this.getClient(firstPlayer), this.getClient(secondPlayer))
            // save game to gameList
            this.games.set(newGame.getUUID(), {
              uuid: newGame.getUUID(),
              playersNames: newGame.getPlayersNames(),
              playersUUIDS: [ firstPlayer, secondPlayer ],
              players: [this.getClient(firstPlayer), this.getClient(secondPlayer)],
              game: newGame
            })
            // remove players from lobby
            this.lobby.delete(firstPlayer)
            this.lobby.delete(secondPlayer)
            // clear variable for next pair
            firstPlayer = null
            secondPlayer = null
          }
        }
        if (value === 'ai') {
          // start game with ai
          // create game instance
          let newGame = new Game(this.getClient(key))
          // save game to gameList
          this.games.set(newGame.getUUID(), {
            uuid: newGame.getUUID(),
            playersNames: newGame.getPlayersNames(),
            playersUUIDS: [firstPlayer],
            players: [this.getClient(firstPlayer), this.getClient(secondPlayer)],
            game: newGame
          })
          // remove player from lobby
          this.lobby.delete(key)
        }
      })
    }, this.READY_CHECK_INTERVAL)
    // check every 2000 sec for finished games and destroy them
    this.finishedGamesChecker = setInterval(() => {
      let games = [ ...this.games.values() ]
      games.map((gameObj) => {
        if (gameObj.game.finished) {
          this.deleteGame(gameObj.uuid)
        }
      })
    }, this.DELETE_FINISHED_GAMES_CHECK_INTERVAL)
  }
  addClient(client) {
    this.clients.set(client.getUUID(), client)
  }
  removeClient(uuid) {
    this.clients.delete(uuid)
  }
  getClient(uuid) {
    return this.clients.get(uuid)
  }
  addToLobby(uuid) {
    this.lobby.set(uuid, 'idle')
  }
  removeFromLobby(uuid) {
    this.lobby.delete(uuid)
  }
  getLobbyInfo() {
    return this.lobby.size
  }
  setStatus(uuid, status) {
    this.lobby.set(uuid, status)
  }
  getActiveGames() {
    // get list of active games
    let games = [ ...this.games.values() ]
    return games.map(({ uuid, playersNames }) => ({ uuid, playersNames }))
  }
  getGame(uuid) {
    return this.games.get(uuid)
  }
  addToSpect(gameUUID, userUUId) {
    // add player to spectate array
    let GameObj = this.games.get(gameUUID)
    GameObj.game.addSpectator(this.getClient(userUUId))
    // delete player from lobby
    this.lobby.delete(userUUId)
  }
  closeGamesFor(userUUID) {
    // user disconnected close game where hi play
    let games = [ ...this.games.values() ]
    games.map((game) => {
      if (game.playersUUIDS.includes(userUUID)) {
        this.deleteGame(game.uuid)
      }
    })
  }
  deleteGame(uuid) {
    // delete game bu uuid
    let game = this.getGame(uuid)
    if (game) {
      game.players.map((player) => {
        if (player && player.finished) {
          player.finished({
            state: 'disconnect'
          })
        }
      })
      if (game && game.spectator && game.spectator.length > 0) {
        game.spectator.map((player) => {
          player.sendSpectEnd({
            state: 'disconnect'
          })
        })
      }
      this.games.delete(uuid)
    }
  }
}

module.exports = new Core()
