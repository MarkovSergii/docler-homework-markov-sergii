const _ = require('lodash')
const uuid = require('uuid')
let Ai = require('./ai')
class Game {
  constructor (firstPlayer, secondPlayer) {
    this.firstPlayer = firstPlayer
    // set players
    // if no second player then first player want play with AI
    // and we crate ai instance
    this.secondPlayer = secondPlayer || new Ai(this)
    this.spectator = []
    // set empty board
    this.board = [[null, null, null],
      [null, null, null],
      [null, null, null] ]
    // list of win combinations
    this.winCombinations = [
      [[0, 0], [0, 1], [0, 2]],
      [[1, 0], [1, 1], [1, 2]],
      [[2, 0], [2, 1], [2, 2]],
      [[0, 0], [1, 0], [2, 0]],
      [[0, 1], [1, 1], [2, 1]],
      [[0, 2], [1, 2], [2, 2]],
      [[0, 0], [1, 1], [2, 2]],
      [[0, 2], [1, 1], [2, 0]]
    ]
    // create game uuid
    this.uuid = uuid()
    this.startGame()
  }

  addSpectator(player) {
    player.spectStart()
    player.sendGameState(this.board)
    this.spectator.push(player)
  }
  getUUID() {
    return this.uuid
  }
  startGame() {
    let sign
    if (this.firstPlayer.getName() === 'TEST_NAME_FOR_TESTS') {
      sign = 1
    } else {
      sign = _.random(0, 1)
    }
    this.sign = {}
    this.finished = false
    this.firstPlayerUUID = this.firstPlayer.getUUID()
    this.secondPlayerUUID = this.secondPlayer.getUUID()
    this.sign[this.firstPlayerUUID] = sign === 1 ? 'x' : 'o'
    this.sign[this.secondPlayerUUID] = sign === 0 ? 'x' : 'o'

    this.firstPlayer.startGame(this.uuid, this.sign[this.firstPlayer.getUUID()], this.secondPlayer.getName())
    this.secondPlayer.startGame(this.uuid, this.sign[this.secondPlayer.getUUID()], this.firstPlayer.getName())
    if (sign === 1) {
      this.firstPlayer.turn()
    } else {
      this.secondPlayer.turn()
    }
  }
  getPlayersNames() {
    return this.firstPlayer.getName() + ' VS ' + this.secondPlayer.getName()
  }
  canSignCell ({ row, column }) {
    return !this.board[row][column]
  }
  checkCombination(arr) {
    // check if current combination arr is win combination
    if (!arr[0]) return false
    if (_.every(arr, (cell) => cell === arr[0])) {
      return arr[0]
    }
  }
  getCellValue (arr) {
    return this.board[arr[0]][arr[1]]
  }
  checkGameEnd() {
    // check all win combination or draw
    for (let i = 0; i <= this.winCombinations.length - 1; i++) {
      let combination = this.winCombinations[i]
      let resultCheckCombination = this.checkCombination([this.getCellValue(combination[0]), this.getCellValue(combination[1]), this.getCellValue(combination[2])])
      if (resultCheckCombination) {
        let winner
        _.mapKeys(this.sign, (value, key) => {
          if (value === resultCheckCombination) { winner = this.firstPlayerUUID === key ? this.firstPlayer.getName() : this.secondPlayer.getName() }
        })

        return {
          result: {
            state: 'winner',
            winner: winner
          },
          end: true
        }
      }
    }
    if (_.every(_.flatten(this.board), (cell) => cell !== null)) {
      return {
        result: {
          state: 'draw'
        },
        end: true
      }
    }
    return {
      end: false
    }
  }
  tryToMove(userUUID, point) {
    // player try to move to sell
    // check if it possible
    // then make move and switch turn
    // or return that player cant do this move
    if (this.canSignCell(point)) {
      this.board[point.row][point.column] = this.sign[userUUID]
      let gameFinished = this.checkGameEnd()

      if (this.firstPlayerUUID === userUUID) {
        this.firstPlayer.move(point, this.sign[this.firstPlayerUUID])
        this.secondPlayer.move(point, this.sign[this.firstPlayerUUID])
      } else {
        this.firstPlayer.move(point, this.sign[this.secondPlayerUUID])
        this.secondPlayer.move(point, this.sign[this.secondPlayerUUID])
      }
      // if there some spectators
      // duplicate commands also to them
      this.spectator.map((player) => {
        player.sendGameState(this.board)
      })
      // send finish commands if game is over
      if (gameFinished.end) {
        this.spectator.map((player) => {
          player.sendSpectEnd(gameFinished.result)
        })

        this.firstPlayer.finished(gameFinished.result)
        this.secondPlayer.finished(gameFinished.result)
        this.finished = true
      } else {
        if (this.firstPlayerUUID === userUUID) {
          this.secondPlayer.turn()
        } else {
          this.firstPlayer.turn()
        }
      }
    } else {
      if (this.firstPlayerUUID === userUUID) {
        this.firstPlayer.cantMove()
      } else {
        this.secondPlayer.cantMove()
      }
    }
  }
}

module.exports = Game
