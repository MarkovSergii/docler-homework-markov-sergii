const tcpSendService = require('../services/tcpSendService')
const uuid = require('uuid')
let core = require('./core')

class Client {
  constructor (connection) {
    this.uuid = uuid()
    this.connection = connection
  }
  getUUID() {
    return this.uuid
  }
  setName(name) {
    this.name = name
  }
  getName(name) {
    return this.name
  }
}
// commands for Tcp client
class ClientTcp extends Client {
  constructor(...arg) { // eslint-disable-line
    super(...arg)
    this.sendUUID()
  }
  sendUUID() {
    let netPackage = { command: 'client:setUUID', data: { uuid: this.uuid } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage))
  }
  sendLobbyInfo() {
    let netPackage = { command: 'client:lobbyInfo', data: { lobbyInfo: core.getLobbyInfo() } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage))
  }
  sendActiveGames() {
    let netPackage = { command: 'client:active-games', data: { activeGames: core.getActiveGames() } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage))
  }
  startGame(uuid, sign, opponentName) {
    let netPackage = { command: 'client:start-game', data: { uuid, sign, opponentName } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage))
  }
  move(point, sign) {
    let netPackage = { command: 'client:move', data: { point, sign } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage))
  }
  cantMove() {
    let netPackage = { command: 'client:cantmove', data: {} }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage))
  }
  turn() {
    let netPackage = { command: 'client:turn', data: {} }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage))
  }
  finished(result) {
    let netPackage = { command: 'client:finished', data: { result } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage))
  }
  sendGameState(board) {
    let netPackage = { command: 'client:sendGameState', data: { board } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage))
  }
  sendSpectEnd(result) {
    let netPackage = { command: 'client:sendSpectEnd', data: { result } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage))
  }
  spectStart() {
    let netPackage = { command: 'client:spectStart', data: { } }
    tcpSendService.write(this.connection, tcpSendService.pack(netPackage))
  }
}
// commands for Web client
class ClientWeb extends Client {
  constructor(...arg) { // eslint-disable-line
    super(...arg)
    this.sendUUID()
  }
  sendUUID() {
    this.connection.send(JSON.stringify({ command: 'client:setUUID', data: { uuid: this.uuid } }))
  }
  sendLobbyInfo() {
    this.connection.send(JSON.stringify({ command: 'client:lobbyInfo', data: { lobbyInfo: core.getLobbyInfo() } }))
  }
  sendActiveGames() {
    this.connection.send(JSON.stringify({ command: 'client:active-games', data: { activeGames: core.getActiveGames() } }))
  }
  startGame(uuid, sign, opponentName) {
    this.connection.send(JSON.stringify({ command: 'client:start-game', data: { uuid, sign, opponentName } }))
  }
  move(point, sign) {
    this.connection.send(JSON.stringify({ command: 'client:move', data: { point, sign } }))
  }
  cantMove() {
    this.connection.send(JSON.stringify({ command: 'client:cantmove', data: {} }))
  }
  turn() {
    this.connection.send(JSON.stringify({ command: 'client:turn', data: {} }))
  }
  finished(result) {
    this.connection.send(JSON.stringify({ command: 'client:finished', data: { result } }))
  }
  sendGameState(board) {
    this.connection.send(JSON.stringify({ command: 'client:sendGameState', data: { board } }))
  }
  sendSpectEnd(result) {
    this.connection.send(JSON.stringify({ command: 'client:sendSpectEnd', data: { result } }))
  }
  spectStart() {
    this.connection.send(JSON.stringify({ command: 'client:spectStart', data: { } }))
  }
}

module.exports = {
  ClientTcp,
  ClientWeb
}
